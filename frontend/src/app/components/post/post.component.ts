import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { UpdatePost } from '../../models/post/update-post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { PostService } from '../../services/post.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { EventService } from '../../services/event.service';
import { GyazoService } from 'src/app/services/gyazo.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;



    public showComments = false;
    public newComment = {} as NewComment;
    public editMode = false;
    public imageUrl: string;
    public imageFile: File;
    public backupImageUrl: string;
    public backupBody: string;
    public updatePost = {} as UpdatePost;
    public loading = false;
    public showFans = false;

    get dislikeCount()
    {
        return this.post.reactions.filter(x=> x.isLike === false).length
    }

    get likeCount()
    {
        return this.post.reactions.filter(x=> x.isLike).length
    }

    get fans()
    {
        var users = this.post.reactions.filter(x=> x.isLike === true).map(function(reaction) {
            return reaction.user.userName;
          });
          return users.join(", ");
    }

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private postService: PostService,       
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private eventService: EventService,
        private gyazoService : GyazoService        
        ) {}

        public ngOnInit() {
        
            this.eventService.commentRemovedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((comment) => {
                this.removeComment(comment);
            });
    
        }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public toggleFanList()
    {
        this.showFans = !this.showFans;
    }

    public sendPost() {

        this.updatePost.body = this.post.body;
        this.updatePost.id = this.post.id;
        
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.updatePost.previewImage = imageData.url;
                    return this.postService.updatePost(this.updatePost);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.editMode = false;
                this.backupBody = undefined;
                this.backupImageUrl =undefined;
                this.loading = false;
            },
            (error) => {
                this.post.previewImage = this.backupImageUrl;
                this.post.body = this.backupBody;
                this.snackBarService.showErrorMessage(error)
            }
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (
                this.post.previewImage = reader.result as string                
                )
            );
        reader.readAsDataURL(this.imageFile);
    }

    public likePost(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post ; 
                this.eventService.postLiked(this.post);
            });

            
    }

    public editPost() {

        this.editMode = !this.editMode;

        if(this.editMode){        
            this.backupImageUrl = this.post.previewImage;
            this.backupBody = this.post.body;
        }
        else if(this.backupBody || this.backupImageUrl) {
            this.post.previewImage = this.backupImageUrl;
            this.post.body = this.backupBody;
        }


    }

    public deletePost() {       
        if(confirm("Are you sure to delete comment: " + this.post.body))
        {
        this.postService
            .deletePost(this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        this.eventService.postRemoved(this.post);                                                     
                        this.snackBarService.showUsualMessage("Post successfully deleted");
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
    }

    public removeImage() {
        this.post.previewImage = undefined;        
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public removeComment(comment: Comment) {
        let indx = this.post.comments.indexOf(comment);

        if (indx>-1) {
            this.post.comments.splice(indx,1);
            
        }
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
