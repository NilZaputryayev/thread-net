import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { LikeService } from '../../services/like.service';
import { empty, Observable, Subject } from 'rxjs';
import { CommentService } from '../../services/comment.service';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;


    private unsubscribe$ = new Subject<void>();
    public editMode = false;
    public showFans = false;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,        
        private snackBarService: SnackBarService,
        private eventService: EventService
    ) {}



    get dislikeCount()
    {
        return this.comment.reactions.filter(x=> x.isLike === false).length
    }

    get likeCount()
    {
        return this.comment.reactions.filter(x=> x.isLike).length
    }

    get fans()
    {
        var users = this.comment.reactions.filter(x=> x.isLike === true).map(function(reaction) {
            return reaction.user.userName;
          });
          return users.join(", ");
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteComment() {       
        if(confirm("Are you sure to delete comment: " + this.comment.body))
        {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                        this.eventService.commentRemoved(this.comment);                                                     
                        this.snackBarService.showUsualMessage("Comment successfully deleted");
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
    }
    
    public updateComment() {       

        this.commentService
            .updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                        this.snackBarService.showUsualMessage("Comment successfully updated");
                        this.editMode = false;
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public toggleEditMode() {
        this.editMode = !this.editMode;
    }  

    public toggleFanList()
    {
        this.showFans = !this.showFans;
    }

    public likeComment(isLike: boolean) {

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp,isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }
}
