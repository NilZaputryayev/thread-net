import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service'
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService : CommentService) {}

    public likePost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        let needChangeState = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === !isLike);

        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerPost.reactions.concat({ isLike: isLike, user: currentUser });

        if(needChangeState) innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
            
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);


        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: isLike, user: currentUser });

                return of(innerPost);
            })
        );
    }

    public likeComment(comment: Comment, currentUser: User, isLike: boolean) {

        const reaction: NewReaction = {
            entityId: comment.id,
            isLike: isLike,
            userId: currentUser.id
        };

        // update current array instantly
        let hasReaction = comment.reactions.some((x) => x.user.id === currentUser.id);
        let needChangeState = comment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === !isLike)

        comment.reactions = hasReaction
            ? comment.reactions.filter((x) => x.user.id !== currentUser.id) 
            : comment.reactions.concat({ isLike: isLike , user: currentUser });

        if(needChangeState) comment.reactions = comment.reactions.concat({ isLike: isLike , user: currentUser });

        hasReaction = comment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.likeComment(reaction).pipe(
            map(() => comment),
            catchError(() => {
                // revert current array changes in case of any error
                comment.reactions = hasReaction
                    ? comment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : comment.reactions.concat({ isLike: true, user: currentUser });

                return of(comment);
            })
        );
    }
}
