import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
import { User } from '../models/user';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';

// tslint:disable:member-ordering
@Injectable({ providedIn: 'root' })
export class EventService {
    private onUserChanged = new Subject<User>();    
    private onPostLiked = new Subject<Post>();
    private onPostRemoved = new Subject<Post>();
    private onCommentRemoved = new Subject<Comment>();

    public postLikedEvent$ = this.onPostLiked.asObservable();
    public postRemovedEvent$ = this.onPostRemoved.asObservable();
    public commentRemovedEvent$ = this.onCommentRemoved.asObservable();
    public userChangedEvent$ = this.onUserChanged.asObservable();
 
    public userChanged(user: User) {
        this.onUserChanged.next(user);
    }

    public postLiked(post: Post) {
        this.onPostLiked.next(post);
    }   
    
    public postRemoved(post: Post) {
        this.onPostRemoved.next(post);
    }  

    public commentRemoved(comment: Comment) {
        this.onCommentRemoved.next(comment);
    }      
}
